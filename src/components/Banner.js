import {Button, Row, Col} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function Banner() {
	return(
		<Row>
			<Col className ="p-5">
				<h1>Booking App-182</h1>
				<p>Enroll courses here!</p>
				<Button variant="primary" as={Link} to="/courses">Enroll Now!</Button>
			</Col>
		</Row>

	)

}