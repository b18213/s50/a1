import{useState, useEffect} from 'react';
import{Row, Col, Card, Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function CourseCard ({courseProp}){
	console.log(courseProp);
	console.log(typeof courseProp);

	// object destructuring
	const {name, description, price, _id} = courseProp
	// console.log(name);

	// Syntax: const [getter, setter] = useState(initialValueofGetter)
	
	// const [count, setCount] = useState(0)
	// const [seat, setSeat] = useState(30)
	// const [isOpen, setIsOpen] = useState(false)

	// const enroll = () =>{

	// 	if(seat > 0){
	// 		setSeat(seat - 1)
	// 		setCount(count + 1)

	// 	}else{
	// 		alert("No more seats!")
	// 	}
	// 	console.log(`Enrolles: ${count}`)
	// 	console.log(`Seats: ${seat}`)		
	// }

	// useEffect(() => {
	// 	if(seat === 0){
	// 		setIsOpen(true)
	// 	}
	// }, [seat])
	

	return(
		<Row className = 'mt-3 mb-3'>
			<Col>
				<Card className="cardHighlight p-3">
					<Card.Body>
						<Card.Title>
							<h5>{courseProp.name}</h5>
						</Card.Title>
						<Card.Subtitle>Description:
						</Card.Subtitle>
						<Card.Text>{courseProp.description}
						</Card.Text>	
						<Card.Subtitle>Price:</Card.Subtitle>
						<Card.Text>{courseProp.price}</Card.Text>
						<Link className="btn btn-primary" to={`/courseView/${_id}`}>View Details</Link>
					</Card.Body>
				</Card>
				
			</Col>
		</Row>
	)
}