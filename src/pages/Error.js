import {Button,Row, Col} from 'react-bootstrap';
import{Link} from	'react-router-dom';

export default function Error() {
	return(
		<Row>
			<Col className ="p-5">
				<h1>ERROR 404 PAGE NOT FOUND</h1>
				<p>The page you are trying to access is not available </p>
				<text>Please go back to the </text>
				<Button className="mb-1 ps-0 text-primary" variant="muted" as={Link} to="/">homepage</Button>
			</Col>
		</Row>

	)

}